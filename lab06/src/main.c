#include <stdio.h>
#define TEXT_LEN 4

int main() {
  char consequence[TEXT_LEN] = { 'A', 'a', 's', 'I' };  //создание масива
  for (int i=0;i<TEXT_LEN;i++) {      //проверка условий для замены
    if (consequence[i] == 'A') {
        consequence[i] = '@';
    }
    if (consequence[i] == 'a') {
        consequence[i] = '@';
    }
    if (consequence[i] == 'O') {
        consequence[i] = '0';

    }
    if (consequence[i] == 'o') {
        consequence[i] = '0';

    }
    if (consequence[i] == 'i') {
        consequence[i] = '1';
    }
    if (consequence[i] == 'I') {
        consequence[i] = '1';
    }
    if (consequence[i] == 's') {
        consequence[i] = '$';
    }
    if (consequence[i] == 'S') {
        consequence[i] = '$';
    }
  }
    for (int i=0;i<TEXT_LEN;i++) {
        printf("%c",consequence[i]);  //воспроизведение результата
    }

  return 0;
}
